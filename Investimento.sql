create database bdSimulaInvest;
go
use bdSimulaInvest;
go
create table tblDados
(
	id int identity(1,1),
    capitalInicial decimal(15,2),
    aporteMensal decimal(15,2),
    taxa decimal(3,2),
    periodo int,
	primary key(id),
);
go

